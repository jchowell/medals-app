## Olymics Medals App

## The Task

1. Fork this repository on Github (if you don't know what that means or how to do it, Google is your friend);
2. Create a *src* folder to contain your code;
3. In the *src* directory, create a simple app to allow a basic Olympic Medals Table to be displayed in a browser or on a device;
4. The app should allow:
 - Countries to be added to a table, along with the TOTAL number of medals the country won during the last Olympic Games; and
 - Rows to be sorted in descending order of medals won, either automatically or manually by the user.

### How to run the app
Run the following commands in the top-level folder:

    npm i
    npm run start

### Linting
A linting script is included using Standard JS and can be run using:

    npm run lint
    npm run lint-fix

### Unit tests
Jest (with Enzyme) unit tests can be run using:

    npm run test

### Future enhancements
* Jest/Enzyme unit tests to test adding of new countries
* if the app data gets more complex, consider using Redux to store the country and medal data
* if client-side validation is required for the country and medals count form, consider moving it to a separate component
