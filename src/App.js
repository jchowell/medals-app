import React, { Component } from 'react'
import { MedalsTable } from './components/MedalsTable'
import './App.css'

class App extends Component {
  render () {
    return (
      <div className='App'>
        <header className='App-header'>
          <h1 className='App-title'>Olympic Medals Table</h1>
        </header>
        <MedalsTable />
      </div>
    )
  }
}

export default App
