/* eslint-env jest */
import React from 'react'
import { mount } from 'enzyme'
import { MedalsTable } from '..'

const mockProps = {
  countries: [
    {
      name: 'Canada',
      totalMedals: 10
    },
    {
      name: 'France',
      totalMedals: 5
    }
  ]
}

describe('Medals Table', () => {
  it('renders with no countries prop', () => {
    const wrapper = mount(<MedalsTable />)
    expect(wrapper.debug()).toMatchSnapshot()
  })

  it('renders with a countries prop', () => {
    const wrapper = mount(<MedalsTable {...mockProps} />)
    expect(wrapper.debug()).toMatchSnapshot()
  })
})
