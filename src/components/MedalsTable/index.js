import React, { Component, Fragment } from 'react'
import PropTypes from 'prop-types'
import './medalsTable.css'

export class MedalsTable extends Component {
  componentDidMount () {
    const { countries } = this.props

    if (Array.isArray(countries) && countries.length) {
      this.setState({
        countries
      }, () => this.sortTableDescending())
    }
  }

  state = {
    countries: [],
    newCountry: {
      name: '',
      totalMedals: ''
    }
  }

  static propTypes = {
    countries: PropTypes.arrayOf(PropTypes.shape({
      name: PropTypes.string,
      totalMedals: PropTypes.number
    }))
  }

  addCountry = (event) => {
    event.preventDefault()
    this.setState({
      countries: [...this.state.countries, this.state.newCountry],
      newCountry: {
        name: '',
        totalMedals: ''
      }
    }, () => this.sortTableDescending())
  }

  /**
   * Update the value in state for controlled form inputs
   * @param {SyntheticEvent} event The React `SyntheticEvent`
   */
  handleInputChange = (event) => {
    const { name, value } = event.currentTarget
    this.setState({
      newCountry: {
        ...this.state.newCountry,
        [name]: value
      }
    })
  }

  /**
   * Sorts the array of countries in descending order
   * by total number of medals.
   */
  sortTableDescending = () => {
    this.setState({
      countries: this.state.countries.sort((a, b) => {
        if (parseInt(a.totalMedals, 10) < parseInt(b.totalMedals, 10)) {
          return 1
        }
        if (parseInt(a.totalMedals, 10) > parseInt(b.totalMedals, 10)) {
          return -1
        }
        return 0
      })
    })
  }

  render () {
    const tableRows = this.state.countries.map((country, id) => {
      return (
        <tr key={id}>
          <td>{country.name}</td>
          <td>{country.totalMedals}</td>
        </tr>
      )
    })

    const newCountryForm = (
      <form className='form--new-country'>
        <input
          autoFocus
          name='name'
          onChange={this.handleInputChange}
          placeholder='Country Name'
          type='text'
          value={this.state.newCountry.name} />
        <input
          name='totalMedals'
          onChange={this.handleInputChange}
          placeholder='Total Medals'
          type='text'
          value={this.state.newCountry.totalMedals} />
        <button onClick={this.addCountry}>Add</button>
      </form>
    )

    return (
      <Fragment>
        <table className='table--medals'>
          <thead>
            <tr>
              <th>Country</th>
              <th>Total medals</th>
            </tr>
          </thead>
          <tbody>
            {tableRows}
          </tbody>
        </table>
        {newCountryForm}
      </Fragment>
    )
  }
}
